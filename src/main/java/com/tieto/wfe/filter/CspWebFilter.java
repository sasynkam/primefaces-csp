package com.tieto.wfe.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Alternative is to add some CSP values to the http header via servlet, but just some that does not break PF and ohter JS
// @WebFilter(urlPatterns = "/*", asyncSupported = true)
public class CspWebFilter implements Filter {

    private static final String CSP_HEADER = "Content-Security-Policy";
    // private static final String CSP_DEFAULT_SRC_SELF = "default-src 'self';";
    private static final String CSP_OBJECT_SRC_SELF = "object-src 'self';";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = ((HttpServletResponse) response);

        if (httpServletResponse.containsHeader(CSP_HEADER)) {
            httpServletResponse.setHeader(CSP_HEADER, CSP_OBJECT_SRC_SELF + httpServletResponse.getHeader(CSP_HEADER));
        } else {
            httpServletResponse.addHeader(CSP_HEADER, CSP_OBJECT_SRC_SELF);
        }
        chain.doFilter(request, response);
    }
}

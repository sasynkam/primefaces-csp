# PrimeFaces - Content Security Policy

This is PrimeFaces app project to test Content Security Policy

## Documentation

* [PrimeFaces Content Security Policy (CSP)](https://www.primefaces.org/showcase/ui/misc/csp.xhtml)
* [Mzilla.org Content-Security-Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/%20Content-Security-Policy)

## Tips

* Install CSP Evaluator extension to the Chrome, click around `index.xhtml` and see result
* See that bundled `mysripts.js` is also safe
